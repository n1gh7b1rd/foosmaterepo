'use strict';

const mongoose = require('mongoose'),
    SensorSchema = require('../models/sensor.js'),
    Schema = mongoose.Schema,
    ItemModel = {
        name: String,
        companyId: String,
        type: String, //TODO: transform to FK?
        description: String,
        sensors: [{ref: 'SensorSchema', type: Schema.Types.ObjectId}]
    },
    ItemSchema = new Schema(ItemModel);

module.exports = mongoose.model('Item', ItemSchema);
