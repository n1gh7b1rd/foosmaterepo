'use strict';

module.exports = function dashboardRoutes(app) {
    app.get('/index', function (req, res) {
        res.render('index');
    });
};