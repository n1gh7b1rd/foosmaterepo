(function() {    
    'use strict';

    angular.module('company', [])
        .factory('CompanyService', CompanyService);

    CompanyService.$inject = ['$http'];
    function CompanyService($http) {
        var service = {};

        service.getCompanyId = getCompanyId;

        return service;
        
        function getCompanyId() {
            return $http({
                method: 'GET',
                url: 'http://localhost:3000/user/getCompany'
            });
        }
    }
})();
