(function() {
    'use strict';

    angular.module('socket', [])
        .factory('SocketService', SocketService);

    SocketService.$inject = ['$rootScope'];
    function SocketService($rootScope) {
        var socket = io.connect(),
            service = {};

        service.on = socket.on.bind(socket);
        service.emit = socket.emit.bind(socket);

        return service;
    }
})();