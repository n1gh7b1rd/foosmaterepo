(function() {
    'use strict';

    angular.module('common', [])
        .controller('MainCtrl', MainCtrl);

        MainCtrl.$inject = ['$scope', 'ConstantsService'];
        function MainCtrl($scope, constants) {
            $scope.constants = constants;
        }

})();