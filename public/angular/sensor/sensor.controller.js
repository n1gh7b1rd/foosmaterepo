(function(){
    'use strict';
    
    angular.module('sensor', [])
        .controller('SensorCtrl', SensorCtrl);

    SensorCtrl.$inject = ['$scope', 'SensorService', 'Constant'];

    function SensorCtrl($scope, SensorService, Constant){
        var self = this;
        self.submitSensor = submitSensor;
        self.deleteSensor = deleteSensor;
        self.updateSensor = updateSensor;
        self.sensorTypes = Constant.SensorTypes;
        self.sensors = [];
        self.defaultType  = self.sensorTypes[0];

        populateSensors();

        function submitSensor(sensor){
            SensorService.createSensor(sensor);
        }

        function updateSensor(sensor){
            SensorService.editSensor(sensor);
        }

        function deleteSensor(sensor){
            SensorService.deleteSensor(sensor);
        }

        function populateSensors(){
            SensorService.getSensors().then(function(result){
                self.sensors = result.data;
            });
        }
    }
})();