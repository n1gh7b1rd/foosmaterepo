'use strict';

module.exports = function crudGenerator(model) {
    let crudOps = {};

    crudOps.create = createDocument;
    crudOps.edit = editDocument;
    crudOps.delete = deleteDocument;

    return crudOps;



    function createDocument(data) {
        let doc = new model(data);
        return doc.save();
    }
      
    function editDocument(data) {
        let byId = { '_id': data._id };

        return model.findOneAndUpdate(byId, data)
            .exec()
            .then(function() {
                console.log(" " + data._id + " updated successfully");
            }, function(err) {
                console.log("Could not update " + Id);
            });
    }


    function deleteDocument(Id) {
        return model.find({ _id: Id})
            .remove()
            .exec()    
            .then(function() {
                console.log(" " + Id + " deleted successfully");
            }, function(err) {
                console.log("Could not delete " + Id);
            });
    }
}


